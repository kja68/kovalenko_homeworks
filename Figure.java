package homeworks;

/**
 * Класс Figure
 * <p>
 * 06.11.2021
 *
  */
public class Figure {
   protected double x;
   protected double y;

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    public double getPerimeter(){
        return 0;
    }

}
