package homeworks;

/**
 * Класс Ellipse
 * <p>
 * 06.11.2021
 *
 *
 */
public class Ellipse extends Figure {
    protected double r1;
    private double r2;

    public Ellipse(double x, double y, double r1, double r2) {
        super(x, y);
        this.r1 = r1;
        this.r2 = r2;
    }
    public double getPerimeter(){
        return (4*3.14*r1*r2-4*(r1-r2))/(r1+r2);
    }
}
