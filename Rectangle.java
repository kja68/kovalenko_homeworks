package homeworks;

/**
 * Класс Rectangle
 * <p>
 * 06.11.2021
 *
 *
 */
public class Rectangle extends Figure{
    protected double a;
    protected double b;

    public Rectangle(double x, double y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public double getPerimeter(){
        return (a * 2 + b * 2);
    }
}

