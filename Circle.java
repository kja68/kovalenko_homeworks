package homeworks;

/**
 * Класс Circle
 * <p>
 * 06.11.2021
 *
 *
 */
public class Circle extends Ellipse{

    public Circle(double x, double y, double r1) {
        super(x, y, r1, r1 );
    }

    public double getPerimeter(){
        return 2*3.14*r1;
    }
}
