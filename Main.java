package homeworks;

public class Main {

    public static void main(String[] args) {
	Figure figure = new Figure(10,20);
    Rectangle rectangle = new Rectangle(10,5, 4, 2);
    Ellipse ellipse = new Ellipse(1,1, 3, 4);
    Square square = new Square(1,1,5);
    Circle circle = new Circle(1, 1,8);
        System.out.println(figure.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(ellipse.getPerimeter());
        System.out.println(square.getPerimeter());
        System.out.println(circle.getPerimeter());
    }
}
